import java.io.File;
import java.util.Scanner;

public class Main {
    public static void main (String[] args) {
        BuscarFichero objBuscarFichero = new BuscarFichero();
        Scanner entrada = new Scanner(System.in);
        String fichero = "";
        String directorio = "";

        System.out.print ("Ingrese el nombre del fichero:\nfichero = " );
        fichero = entrada.next();
        System.out.print ("\nDirectorio de inicio de la busqueda:\ndirectorio = ");
        directorio = entrada.next();
        System.out.println();
        objBuscarFichero.buscar (fichero, new File(directorio));
    }
}
