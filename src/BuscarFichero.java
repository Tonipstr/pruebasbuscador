import java.awt.*;
import java.io.File;
import java.io.IOException;

public class BuscarFichero {
    public void buscar (String argFichero, File argFile) {
        File[] lista = argFile.listFiles();

        if (lista != null) {
            for (File elemento : lista) {
                if (elemento.isDirectory()) {
                    buscar (argFichero, elemento);
                } else if (argFichero.equalsIgnoreCase(elemento.getName())) {
                    System.out.println (elemento.getAbsolutePath());
                    abrirarchivo( elemento.getAbsolutePath());
                }
            }
        }
    }
    public void abrirarchivo(String archivo){
        try {
            File objetofile = new File (archivo);
            Desktop.getDesktop().open(objetofile);
        }catch (IOException ex) {
            System.out.println(ex);
        }

    }
}